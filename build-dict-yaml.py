import requests
import re

tongyong_guifan_hanzi_biao_url = 'https://gitlab.com/rodrigomorales1/tongyong-guifan-hanzi-biao/-/raw/main/list.txt'
tongyong_guifan_hanzi_biao_characters = requests.get(tongyong_guifan_hanzi_biao_url).text.rstrip().split('\n')

wubi86_dict_yaml_url = 'https://raw.githubusercontent.com/rime/rime-wubi/refs/heads/master/wubi86.dict.yaml'
wubi86_dict_yaml_lines = requests.get(wubi86_dict_yaml_url).text.rstrip().split('\n')
wubi86_dict_yaml_index = 0
wubi86_dict_yaml_filtered = open('wubi86_8105.dict.yaml', 'w')

while wubi86_dict_yaml_lines[wubi86_dict_yaml_index] != '...':
  # TODO: Register filter in the changelog that appears at the top of *.dict.yaml
  if re.search('wubi86', wubi86_dict_yaml_lines[wubi86_dict_yaml_index]):
    wubi86_dict_yaml_filtered.write(wubi86_dict_yaml_lines[wubi86_dict_yaml_index].replace('wubi86', 'wubi86_8105'))
  else:
    wubi86_dict_yaml_filtered.write(wubi86_dict_yaml_lines[wubi86_dict_yaml_index])
  wubi86_dict_yaml_filtered.write('\n')
  wubi86_dict_yaml_index += 1

wubi86_dict_yaml_filtered.write('...\n')
wubi86_dict_yaml_filtered.write('\n')

while wubi86_dict_yaml_index < len(wubi86_dict_yaml_lines):
  if wubi86_dict_yaml_lines[wubi86_dict_yaml_index].startswith('#'):
    wubi86_dict_yaml_index += 1
    continue
  if result := re.search(r'^([^	]+)	', wubi86_dict_yaml_lines[wubi86_dict_yaml_index]):
    characters = result.group(1)
    all_belong = True
    for character in characters:
      if character not in tongyong_guifan_hanzi_biao_characters:
        all_belong = False
    if all_belong:
      wubi86_dict_yaml_filtered.write(wubi86_dict_yaml_lines[wubi86_dict_yaml_index])
      wubi86_dict_yaml_filtered.write('\n')
  wubi86_dict_yaml_index += 1
